from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
import time
import string
import random

EMAIL_ID = 'andrew444444@gmail.com'
NAME = 'John Doe'

# delay of input for mimic human behavior
def slow_typing(element, text):
   for character in text: 
      element.send_keys(character)
      time.sleep(0.3)

def random_password(length):
    lower = string.ascii_lowercase
    upper = string.ascii_uppercase
    num = string.digits
    symbols = string.punctuation

    all = lower + upper + num + symbols
    temp = random.sample(all,length)
    password = "".join(temp)
    return password

browser = webdriver.Firefox()

browser.get('https://www.browserstack.co')

url = browser.current_url

if (url != 'https://www.browserstack.com/'):
    browser.close()
    raise Exception('Redirection was unsuccessful')
print(browser.title)

time.sleep(2)

cookie_cta = browser.find_element_by_id('accept-cookie-notification')
cookie_cta.click()

try:
    sign_up = browser.find_element_by_xpath("//*[text()='Get started free' or text()='Sign Up' or text()='Create new account']")
except NoSuchElementException:
    browser.close()
    raise Exception('No sign up button was found')

button = browser.find_element_by_id('signupModalButton')
button.click()

time.sleep(2)

username = browser.find_element_by_id('user_full_name')
slow_typing(username, NAME)

time.sleep(1)

email = browser.find_element_by_id('user_email_login')
slow_typing(email, EMAIL_ID)

time.sleep(2)

password = browser.find_element_by_id('user_password')
password_string = random_password(8)
slow_typing(password, password_string)

time.sleep(1)

toc = browser.find_element_by_name('terms_and_conditions')
toc.click()

signupbutton = browser.find_element_by_id('user_submit')
signupbutton.click()

time.sleep(10)

try:
    sign_up = browser.find_element_by_xpath(f"//*[contains(normalize-space(), 'Hi {NAME.split()[0]}')]")
except NoSuchElementException:
    browser.close()
    raise Exception("Account wasn't created")

browser.close()